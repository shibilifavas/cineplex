const root = document.getElementById("root");
const seatArray = [
    false,
    false,
    false,
    false,
    true,
    false,
    false,
    false,
    false,
    false
];
for(let i = 0; i < seatArray.length; i++){
    const newSeat = document.createElement("div");
    newSeat.classList.add("seat");
    if (seatArray[i] === true) newSeat.classList.add("booked");
    root.appendChild(newSeat);
}

//# sourceMappingURL=index.c36f364e.js.map
